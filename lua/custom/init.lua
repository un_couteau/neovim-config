-- We are just modifying lspconfig's packer definition table
-- Put this in your custom plugins section i.e M.plugins field 

["neovim/nvim-lspconfig"] = {
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.plugins.lspconfig"
    end,
}
