return
{

  ["tombh/novim-mode"] = {event = "VimEnter"},
  ["williamboman/mason-lspconfig.nvim"] = {},
  ["neovim/nvim-lspconfig"] =
  {
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.plugins.lspconfig"
    end,
  },
  ["lambdalisue/suda.vim"] = {}
}
